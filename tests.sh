#! /usr/bin/env bash
# SPDX-License-Identifier: MPL-2.0
# Copyright (c) 2020 Francis Laniel <francis.laniel@lip6.fr>


TESTS_DIR='tests'
TESTS=("${TESTS_DIR}/destination.sh"
"${TESTS_DIR}/simple_conversion.sh"
"${TESTS_DIR}/no_export_area_conversion.sh"
"${TESTS_DIR}/highest_number.sh"
"${TESTS_DIR}/conversions.sh"
)

for test in ${TESTS[@]}; do
	if ! bash $test; then
		exit 1
	fi
done