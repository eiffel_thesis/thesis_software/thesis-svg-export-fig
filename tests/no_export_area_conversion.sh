#! /usr/bin/env bash
# SPDX-License-Identifier: MPL-2.0
# Copyright (c) 2020 Francis Laniel <francis.laniel@lip6.fr>


ret=0

# When inkscape build a PDF, it adds creation time inside the file.
# I set SOURCE_DATE_EPOCH to a magic value, so I can compare the PDF produced.
SOURCE_DATE_EPOCH=1595430542
export SOURCE_DATE_EPOCH

# First we create the PDF with default inkscape
# We do not use '--export-area-drawing' so we can test -n option of script.
inkscape svg/python.svg --without-gui --export-pdf=inkscape.pdf
python3 svg_export_fig.py -n svg/python.svg -d .

if ! diff python.pdf inkscape.pdf; then
	echo 'Both files, python.pdf and inkscape.pdf, must be equal' 1>&2

	ret=1
fi

rm inkscape.pdf python.pdf

exit $ret