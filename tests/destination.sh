#! /usr/bin/env bash
# SPDX-License-Identifier: MPL-2.0
# Copyright (c) 2020 Francis Laniel <francis.laniel@lip6.fr>


ret=0

mkdir pdf

before_script=$(ls pdf)

python3 svg_export_fig.py svg/python.svg -d pdf

after_script=$(ls pdf)

# After the script, the PDF file must be in pdf, so the two ls must differ.
if [ "${before_script}" = "${after_script}" ]; then
	echo "${before_script} must not be equal to ${after_script}" 1>&2

	ret=1
fi

rm -r pdf

exit $ret