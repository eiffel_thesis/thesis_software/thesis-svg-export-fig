#! /usr/bin/env bash
# SPDX-License-Identifier: MPL-2.0
# Copyright (c) 2020 Francis Laniel <francis.laniel@lip6.fr>


# This array contains string which are the following:
# "name_of_the_svg_file_to_test number_of_pdf_generated sha1_sums_for_each_pdf"
# I checked all the generated files to see if they correspond to that the script
# should return.
# I generated them after setting SOURCE_DATE_EPOCH before writing this script.
# Then I computed their sha1 sum.
# So, this script should fail if the computed pdf do not match with these sums.
CONVERSIONS=(
"simply_layered 3 09edc281c5c846512d68298f9fd45e80f2e7de71 c1aed0e154f4dbaff1600f587ffd0987c313e114 78572a4fc6ef3ba2a2d040a9c4e60007b85cffa3"
"list_layered 3 09edc281c5c846512d68298f9fd45e80f2e7de71 2f149f9be89e7ee0bcc852bb9ea43f7c78334de3 a6e6c574693af6cfdf939976d01b1ad5b7ffb77a"
"interval_layered 3 09edc281c5c846512d68298f9fd45e80f2e7de71 2f149f9be89e7ee0bcc852bb9ea43f7c78334de3 a6e6c574693af6cfdf939976d01b1ad5b7ffb77a"
"mix_layered 2 a6e6c574693af6cfdf939976d01b1ad5b7ffb77a 2f149f9be89e7ee0bcc852bb9ea43f7c78334de3"
"complicated_layered 9 a6e6c574693af6cfdf939976d01b1ad5b7ffb77a a6e6c574693af6cfdf939976d01b1ad5b7ffb77a a6e6c574693af6cfdf939976d01b1ad5b7ffb77a 2f149f9be89e7ee0bcc852bb9ea43f7c78334de3 a6e6c574693af6cfdf939976d01b1ad5b7ffb77a a6e6c574693af6cfdf939976d01b1ad5b7ffb77a 2f149f9be89e7ee0bcc852bb9ea43f7c78334de3 a6e6c574693af6cfdf939976d01b1ad5b7ffb77a
a6e6c574693af6cfdf939976d01b1ad5b7ffb77a"
)

SOURCE_DATE_EPOCH=1595430542
export SOURCE_DATE_EPOCH

# Run a test of conversion.
# @param name The name of the test and of the file to be converted.
# @param number The number of pdf files which will be generated.
# @param shas The sha1 sums of the PDF files.
function unit_test {
	if [ $# -lt 3 ]; then
		echo "${FUNCNAME[0]} needs three arguments: the name, the number and the sha1 sums" 1>&2

		exit 1
	fi

	name=$1
	number=$2

	# We get the two first arguments so we need to remove them from $@ with shift.
	shift 2

	# The sha1 sums are the rest of the arguments
	shas=($@)

	python3 svg_export_fig.py "svg/${name}.svg" -d .

	# C for-loop style yeah!
	for ((i = 1; i < number; i++)); do
		filename="${name}-fig${i}.pdf"

		sha1=$(sha1sum $filename | awk '{print $1}')

		# Array begins to 0, so we need to substract 1 to $i.
		j=$(($i - 1))

		if [ "${sha1}" != "${shas[$j]}" ]; then
			echo "Generated file (${filename}) does not correspond to what it should: '$sha1' != '${shas[$j]}'" 1>&2

			rm *.pdf

			exit 1
		fi
	done
}

for conversion in "${CONVERSIONS[@]}"; do
	unit_test $conversion
done

rm *.pdf