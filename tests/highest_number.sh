#! /usr/bin/env bash
# SPDX-License-Identifier: MPL-2.0
# Copyright (c) 2020 Francis Laniel <francis.laniel@lip6.fr>


SVGS=('svg/python.svg'
'svg/simply_layered.svg'
'svg/list_layered.svg'
'svg/interval_layered.svg'
'svg/mix_layered.svg'
'svg/complicated_layered.svg'
)

EXPECTED_LAST_LEVEL=('0' '3' '3' '3' '2' '9')

for i in ${!SVGS[@]}; do
	highest=$(python3 svg_export_fig.py -v ${SVGS[i]} -d . | head -1 | awk '{print $3}')
	expected=${EXPECTED_LAST_LEVEL[i]}

	if [ $highest != $expected ]; then
		echo "Highest figure number must be ${expected} and is ${highest}" 1>&2

		rm *.pdf

		exit 1
	fi
done

rm *.pdf

# This one must fail.
if python3 svg_export_fig.py svg/badly_layered.svg -d .; then
	echo 'svg/badly_layered.svg is badly labeled and script must returns non 0' 1>&2

	exit 1
fi